@extends ('layouts.master')

@section ('content')
	<h2>Register</h2>
	<hr>
	@include ('layouts.errors')
	<form action="{{ url('/register') }}" method="POST" role="form">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" class="form-control" id="name" name="name">
		</div>
		<div class="form-group">
			<label for="email"">Email</label>
			<input type="email" class="form-control" id="email"" name="email"">
		</div>
		<div class="form-group">
			<label for="password"">Password</label>
			<input type="password" class="form-control" id="password"" name="password"">
		</div>
		<div class="form-group">
			<label for="password_confirmation"">Password</label>
			<input type="password" class="form-control" id="password_confirmation"" name="password_confirmation"">
		</div>
		<button type="submit" class="btn btn-primary">Register</button>
	</form>
@endsection