@extends ('layouts.master')

@section('content')

	<h1>Publish a post</h1>
	<hr>
	@include ('layouts.errors')
	<form method="POST" action="{{ url('/posts') }}" enctype="multipart/form-data">
	{{ csrf_field() }}
	  <div class="form-group">
	    <label for="title">Title</label>
	    <input type="text" class="form-control" id="title" name="title">
	  </div>
	  <div class="form-group">
	    <label for="body">Body</label>
	    <textarea name="body" id="body" class="form-control" rows="3"></textarea>
	  </div>
	  <div class="form-group">
	    <label for="body">Img</label>
	    <input type="file" name="img_upload" id="input" class="form-control">
	  </div>
	  <button type="submit" class="btn btn-primary">Publish</button>
	</form>
@endsection