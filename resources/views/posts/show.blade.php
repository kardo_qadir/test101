@extends ('layouts.master')

@section ('content')
<div class="blog-post">
	<h2 class="blog-post-title">{{ $post->title }}</h2>
	<p class="blog-post-meta">{{ $post->created_at->diffForHumans()  }}</p>
	{{ $post->body }}
	<hr>
	<div class="commets">
		<ul class="list-group">
			@foreach ($post->comments as $comment)
				<li class="list-group-item">
					<strong>{{ $comment->created_at->diffForHumans() }}: </strong>
					{{ $comment->body }}
				</li>
			@endforeach
		</ul>
	</div>
	<br><br>
	<form method="POST" action="{{ url('/posts/'.$post->id.'/comment') }}">
		{{ csrf_field() }}
	  <div class="form-group">
	    <label for="body">Write a comment</label>
	    <textarea name="body" id="body" class="form-control" rows="3"></textarea>
	  </div>
	  <button type="submit" class="btn btn-primary">Comment it!</button>
	</form>
	@include ('layouts.errors')
</div><!-- /.blog-post -->
@endsection