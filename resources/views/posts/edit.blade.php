@extends('layouts.master')
@section('title','Edit Post')
@section('content')
	<h1>Edit the post</h1>
	<hr>
	@include ('layouts.errors')
	<form method="POST" action="{{ url('posts/'.$post->id) }}">
	{{ method_field('PATCH') }}
	{{ csrf_field() }}
	  <div class="form-group">
	    <label for="title">Title</label>
	    <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
	  </div>
	  <div class="form-group">
	    <label for="body">Body</label>
	    <textarea name="body" id="body" class="form-control" rows="3">{{ $post->body }}</textarea>
	  </div>
	  <button type="submit" class="btn btn-primary">Edit</button>
	</form>
@endsection