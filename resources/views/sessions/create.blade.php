@extends ('layouts.master')

@section ('content')
	<h2>Login</h2>
	<hr>
	@include('layouts.errors')
	<form action="{{ url('/login') }}" method="POST" role="form">
	{{ csrf_field() }}
		<div class="form-group">
			<label for="password">Email</label>
			<input type="text" class="form-control" id="email" name="email" placeholder="jan@example.com">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password" name="password" placeholder="****">
		</div>		
	
		<button type="submit" class="btn btn-primary">Sign In</button>
	</form>
@endsection