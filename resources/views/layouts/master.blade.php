<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog - @yield('title')</title>
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('/css/app.css') }}" rel="stylesheet">
    @yield('style')
  </head>

  <body>

    @include('layouts.header')
    

    <div class="container">

      <div class="row">
        <div class="col-sm-8 blog-main">
          @yield('content')
        </div>
        <div class="col-sm-3 offset-sm-1 blog-sidebar">
          @include('layouts.sidebar')
        </div>

      </div><!-- /.row -->

    </div><!-- /.container -->
    @if($flash = session('message'))
    <div class="alert alert-success" style="position: absolute;bottom: 0;right: 0;">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      {{ $flash }}
    </div>
    @endif
    @include('layouts.footer')
  </body>
</html>
