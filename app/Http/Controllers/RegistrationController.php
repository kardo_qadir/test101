<?php 
namespace App\Http\Controllers;

use App\Http\Requests\RegistartionRequest;
use Illuminate\Http\Request;
use App\User;
use App\Mail\NewEmail;
// use Illuminate\Support\Facades\Mail;
class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function create(){
    	return view('registration.create');
    }

    public function store(RegistartionRequest $request){
		// $user = User::register(['name','email','passwrod']);
    	$user = User::register(['name','email','password']);

    	if(! auth()->attempt(request(['email','password'])) ){
           return redirect()->back();
        }

        \Mail::to($user)->send(new NewEmail($user));

    	return redirect()->home();
    }
}
