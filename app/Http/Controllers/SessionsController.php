<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NewEmail;
class SessionsController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest',['except'=> 'destroy']);
	}

	/* Loin Form */
    public function create(){
    	return view('sessions.create');
    }

    /* Log In */
    public function store()
    {
    	$this->validate(request(),[
    		'email' => 'required|email',
    		'password' => 'required',
    		]);
    	if(! auth()->attempt(request(['email','password']))) {
    		return redirect()->back()->withErrors([
    			'message' => 'Please enter correct information to login!'
    			]);
    	}

        \Mail::to($user = auth()->user())->send(new NewEmail($user));
    	return redirect()->home();
    }
    
    /* Log Out */
    public function destroy()
    {
    	auth()->logout();
    	return redirect()->home();	
    }
}
