<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Carbon\Carbon;
use App\Http\Requests\PostsRequest;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    public function index(){
        session('message');
        $posts = Post::latest()
                ->filter(request(['month','year']))
                ->get();
    	return view('posts.index',compact('posts'));
    }

    public function show(Post $post){
        return view('posts.show',compact('post'));
    }

    public function create()
    {
    	return view('posts.create');
    }


    public function store(PostsRequest $request)
    {

        auth()->user()->publish(
            new Post(request(['title','body']))
            );
        session()->flash('message','You have added a new post some seconds ago! ');
    	return redirect('/');
    }

    public function edit(Post $post)
    {
        return view('posts.edit',compact('post'));
    }

    public function update(PostsRequest $request,Post $post)
    {
        // dd($id);
        // $post->update([
        //     'user_id'=> auth()->id(),
        //     'title' => $request->title,
        //     'body' => $request->body,
        //     ]);
       auth()->user()->edit(request(['title','body']));
        return redirect()->back();
    }

}
