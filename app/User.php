<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    public function publish($post)
    {
        $this->posts()->save($post);
    }
    public function edit($post = [])
    {
        $this->posts()->update($post); 
    }
    public static function register($user = [])
    {
        return static::create([
            'name' => request($user['name']),
            'email' => request($user['email']),
            'password' => bcrypt(request($user['password'])),
            ]);
    }

    public function commentOn(Post $post,$body)
    {
        return (new Comment(compact('body')))
            ->user()->associate($this)
            ->post()->associate($post)
            ->save();
    }
}
